<?php 
/***************************************************************
 *  File Name : Add New Builder
 *  Created Date: 14/06/2016
 *  Created By: Prasad Bhale
 ************************************************************** */


/* Including Globally Declared Variables */
include_once("config/config.php");


$tab="Manage Acts";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array()
					  );

// Include Common Files
//include_once(CONFIG_CLASS_PATH ."class.php");

//Include Controller Section
//include(CONTROLLER_PATH."ManageCategoryController.php");

/* Include message.php file */
//include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';
  
$title="Manage Acts";
$sub_title="Create Acts";


// Include Header Section
include(NAVIGATION_FILE . "header.php");

//Include View Section
include(VIEW_PATH."manage_acts_html.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
