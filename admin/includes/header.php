<!DOCTYPE html>
<html>
  <head>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cj Farm - Admin Portal</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    
<!-- Ionicons -->

    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="plugins/jquery-confirm-master/css/jquery-confirm.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="css/desi_farm_admin.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
<style>
#message_div{
    margin-bottom: 0px;
    margin-top: 50px;
}
.red{color:red;}
	
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    bottom: 100%;
    left: 50%;
    margin-left: -60px;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}

    .show{font-size: large;
	}
.form-inline{
    padding: 20px;
}
</style>
<?php	

//if(!isset($_SESSION['email_id']) && $_SESSION['email_id']== "") 
	//  header('Location:index.php');
	  
	  
	/* Including the common_includes_css.php to include all css files in Add_css array */
	//include_once(NAVIGATION_PATH . "common_includes_css.php");

?>
  </head>
  <body class="sidebar-mini skin-blue fixed">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
         <!--<span class="logo-mini"><img width="50px" height="50" src="images/desifarms_logo_clean (1).png"></img></span>
          <!-- logo for regular state and mobile devices  desifarms_logo_clean (1).png-->
          <!-- <span class="logo-lg pull-left"><img width="50px" height="50" src="images/desifarms_logo_clean (1).png"></img><b class="pull-right"> &nbsp;&nbsp;  Admin</b></span>-->
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- Notifications: style can be found in dropdown.less -->

              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">0</span> 
				</a>

                <ul class="dropdown-menu">
                  <li class="header">You have 0 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                     </ul>
                  </li>
                  <li class="footer"><a href="manage_notification.php">View all</a></li>
                </ul>
              </li>

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                  <span class="hidden-xs">Welcome Admin </span>
                </a>

                <ul class="dropdown-menu">
                  <!-- User image -->
                <li class="user-header">
                    <p>CJ Farm</p>
                </li>
                 
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="index.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="index.php"><i class="fa fa-power-off"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>
	  
      <!-- Left side column. contains the logo and sidebar -->
 <?php 
		if(count($Messages) >0) 
		{       
			//	echo implode("<br/>",$Messages); 
				// $_SESSION['Message']=0 ;
				//die;
				
		} 
		
	 ?>
      <aside class="main-sidebar">
	  
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
		  
          
           <?php 
            include_once("left_menu.php");
           ?>

        </section>
        <!-- /.sidebar -->
      </aside>
