<?php
/* * ************************************************************
*  File Name : common_includes_js.php
*  File Description: includes all js files which are sent in AddJS array of Motherwhood Project.
*  Author: Benchmark, 
*  Created Date: 24/10/2013
*  Created By: Raju Tikale
* ************************************************************* */
  
	if (isset($AddJS) && count($AddJS) > 0) 
    {
        $files_count = count($AddJS);
        for ($JS = 0; $JS < $files_count; $JS++) 
        {
            ?>
                <script language="javascript" type="text/javascript" src="<?php echo JS_PATH . $AddJS[$JS] ?>.js"></script>
            <?php
        }
    }
?>	
		