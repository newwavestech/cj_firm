<?php
session_start();
error_reporting(1);
 date_default_timezone_set('Asia/Kolkata');
if(!isset($_SESSION['user']) && $tab!="User Login")
{
 /*echo "<script>window.location='index.php';</script>";*/
}


require_once(dirname(__FILE__) . "/config.base.php");

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 0);

define("ITEMS_PER_PAGE",10);
define("ITEMS_PER_PAGE_ACTIVITY",2);



define("FOLDER_PATH", BASE_PATH);
define("FOLDER_PATH_HTTP", BASE_URL);



//======== CONTROLLER_PATH PATH =============//
define("CONTROLLER_PATH", FOLDER_PATH . "controller/");
define("CONTROLLER_PATH_HTTP", FOLDER_PATH_HTTP . "controller/");



//======== CONTROLLER_PATH PATH AJAX=============//
define("AJAX_PATH", FOLDER_PATH . "controller/ajax/");
define("AJAX_PATH_HTTP", FOLDER_PATH_HTTP . "controller/ajax/");

//======== CSS PATH =============//
define("CSS_PATH_HTTP", FOLDER_PATH_HTTP . "css/");
//======== IMAGE PATH =============//
define("IMAGE_PATH_HTTP", FOLDER_PATH_HTTP . "images/");

//======== VIEW PATH =============//
define("VIEW_PATH", FOLDER_PATH . "view_page/");
define("VENDOR_VIEW_PATH", FOLDER_PATH . "views/vendor/");



//======== class config Path =============//
define("CONFIG_CLASS_PATH", BASE_PATH . "config/");

//======== NAVIGATION PATH =============//
define("NAVIGATION_FILE", FOLDER_PATH . "includes/");
define("JS_PATH", FOLDER_PATH_HTTP . "js/");
define("MODULE_PATH", FOLDER_PATH . "includes/modules/");
define("MODEL_DIR_PATH", FOLDER_PATH . "model/");
define("MODULE_PATH_HTTP", FOLDER_PATH_HTTP . "includes/modules/");
define("INCLUDE_DIR_PATH", FOLDER_PATH . "includes/");
define("MODULES_DIR_PATH", INCLUDE_DIR_PATH . "modules/");
define("FUNCTION_DIR_PATH", INCLUDE_DIR_PATH . "functions/");
define("ADMIN_DIR_PATH", FOLDER_PATH . "admin/");
define("CLIENT_DIR_PATH", FOLDER_PATH . "client/");
define("ADMIN_COMMON_FILE_PATH", INCLUDE_DIR_PATH . "navigation/admin/");
define("CLIENT_COMMON_FILE_PATH", INCLUDE_DIR_PATH . "navigation/client/");

define("CONTROLLER_PATH", FOLDER_PATH . "controller/admin/");
define("ADMIN_CONTROLLER_PATH_HTTP", FOLDER_PATH_HTTP . "controller/admin/");

//Pagination Admin
define("INCREMENT_BY", 10);

//======== SENDEMAIL PATH =============//
define("EMAIL_DIR_PATH", MODULES_DIR_PATH . "email/");

define('CURR_DATE', date('Y-m-d H:i:s'));

/* HTML5 validations patterns */
/* validation pattern for text field */
define("ONLY_TEXT_PATTERN","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_&\s]+)*$");
define("ONLY_TEXT_MESSAGE","Only Alhanumeric,(),- ,_,& allowed. ");

//define("TEXT_PATTERN","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_'&\s]+)*$");
define("TEXT_PATTERN1","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_&']+)*$");
define("TEXT_PATTERN","^[-@./#&+\w\s]*$");

define("TEXT_MESSAGE","Provide alphanumeric value");

define("DESCRIPTION_PATTERN","[a-zA-Z0-9()-']");
define("DESCRIPTION_MESSAGE","Only Alhanumeric,(),- ,' allowed. ");


//define("EMAIL_PATTERN","[a-zA-Z0-9.@-_]");
define("EMAIL_PATTERN","[^@]+@[^@]+\.[a-zA-Z]{2,6}");
 
define("EMAIL_MESSAGE","Provide proper Email Address");

define("NUMBER_PATTERN_3_DIG",".{3,}");
define("NUMBER_PATTERN_3_MSG","Minimum 3 digit require");

define("NUMBER_PATTERN_4_DIG",".{4,}");
define("NUMBER_PATTERN_4_MSG","Minimum 4 digit require");


define("FLOAT_PATTERN","[0-9.,]");
define("FLOAT_PATTERN_MSG","Only Integer / Float Value allowed");

define("NUMBER_PATTERN","[0-9]*");
define("NUMBER_PATTERN_MSG","Only Integer allowed");

define("FLOAT_PATTERN_2","[0-9]+([\.|,][0-9]+)?");



define("NUMBER_PATTERN_GRETER","^[1-9][0-9]*$");
define("NUMBER_PATTERN_GRETER_MSG","number should be greater than zero");

/* validation pattern for url */
define("URL_PATTERN","(http|https|ftp)://www\.([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])|([a-zA-Z0-9-]+\.)+(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(/($|[a-zA-Z0-9.,?'\\+&%$#=\~_-]+))*");
define("URL_PATTERN_MSG","Provide proper url");

//========== Logo image ============//

define('YEAR_RANGE', date('Y')); 

define("MONTH_ARRAY", serialize(array(1 =>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December')));

define("SMS_CONTENT_ARRAY", serialize(array(1 =>'Invoice Generate',2=>'Customer Registration',3=>'Customer Bill')));

define("GENDER", serialize(array('0' =>'---Select Gender---','1' =>'Male','2'=>'Female')));

// LAnguage Array


define("TXTMAXLENGTH", 30);


//======= BOOK_THUMBNAIL Folder Path =========// 
define("BOOK_THUMBNAIL_FOLDER_HTTP", FOLDER_PATH_HTTP."images/book_thumbnails/");
define("BOOK_THUMBNAIL_FOLDER_PATH", FOLDER_PATH."images/book_thumbnails/");

// Export CSV
define("EXPORT_DIR_PATH",FOLDER_PATH."report_files/export/");
define("EXPORT_FILE_PATH",FOLDER_PATH_HTTP."report_files/export/");

// Import Blogger CSV
define("IMPORT_CSV_PATH", BASE_PATH . "user_files/imported_csv");

define("EXT",".php");

define("USER_TYPE", serialize(array(1=>'Admin',2=>'General User')));

// Lead Source
define("LEAD_SOURCE", serialize(array(1=>'Online Portal',2=>'Walk In',3=>'Sub-Brokers',4=>'Newspaper',5=>'Exhibition')));

// Property Type
define("PROPERTY_TYPE", serialize(array(1=>'Outright',2=>'Rental')));

// Proprty Type
define("TYPE_OF_PROPERTY", serialize(array(1=>'Flat',2=>'Row House',3=>'Independent House',4=>'Commercial Space',5=>'Commercial Shop',6=>'Land')));

// Area
define("AREA", serialize(array(1=>'Sq Ft',2=>'Sq Mtrs')));

// BHK
define("BHK", serialize(array(1=>'1RK',2=>'1 BHK',3=>'2 BHK',4=>'3 BHK',5=>'4 BHK',6=>'5 BHK')));

// Amenities
define("AMENITIES", serialize(array(1=>'Play Area',2=>'Parking',3=>'Club House',4=>'Swimming Pool',5=>'Others')));


// Customer Type
define("CUSTOMER_TYPE", serialize(array(1=>'Buyer',2=>'Seller',3=>'Tenant')));

// Terms
define("TERM", serialize(array(1=>'11 Month',2=>'1 Year',3=>'2 Year',4=>'3 Year')));

// Documents
define("DOCUMENTS", serialize(array(1=>'Agreement',2=>'PAN Card',3=>'ID Proof',4=>'Address Proof',5=>'Police Verification',6=>'Electricity Bill',7=>'Power of Attorney',8=>'NOC',9=>'Corp Tax-',10=>'Co-owner Pan Card',11=>'Photo')));

//Lead Status
define("LEAD_STATUS", serialize(array(1=>'Positive',2=>'Negative',3=>'Pending')));

//Expense Type
define("EXPENSE_TYPE", serialize(array(1=>'Tea/Coffee',2=>'Snacks/ Meal',3=>'Xerox',4=>'Legal',5=>'Fuel',6=>'Goods Transport',7=>'Painting',8=>'Plumbling',9=>'Cleaning',10=>'Electricity Bill',11=>'Maintainence',12=>'Corporation Tax',13=>'Other')));

define("PHOTO_UPLOAD_HTTP_PATH", "http://".$_SERVER['SERVER_NAME']."/thb/admin/uploaded_files/" );  // Photo Upload Path
define("CLIENT_SIGN_UPLOAD_HTTP_PATH", "http://".$_SERVER['SERVER_NAME']."/thb/admin/client_signatures/" );  // Client Signatures http


// Subscriber Type 
//0=>'None',1=>'Daily',2=>'Week Days',3=>'Customize',4=>'Alternate Days'
define("SUBSCRIBER_TYPE", serialize(array(0=>'None',1=>'Daily',2=>'Week Days',3=>'Customize',4=>'Alternate Days')));

// Milk Type  
define("MILK_TYPE", serialize(array(0=>'Nothing',1=>'Cow Milk',2=>'Buffalow Milk',3=>'Gir Milk')));


// Access Rights 
define("ACCESS_RIGHT", serialize(array('1'=>'Dashboard','2'=>'Setting','3'=>'Manage Product','4'=>'Manage Customer','5'=>'Manage Order','6'=>'Manage Subscription','7'=>'Hold Subscription','8'=>'Manage Invoice','9'=>'Manage Notification','10'=>'My Profile','11'=>'Manage Payment','12'=>'Todays Orders','13'=>'Tomorrows Orders')));


// Product Images Upload Path
define("PRODUCT_DIR_PATH",FOLDER_PATH."images/product_images/");
define("PRODUCT_DIR_HTTP_PATH",FOLDER_PATH_HTTP."images/product_images/");


// DB Con
define("DB_HOST","localhost");
define("DB_USER","root");
define("DB_PASSWORD","");
define("DB_DATABASE","");


define("INVOICE_FILE_FOLDER",FOLDER_PATH_HTTP."save invoice pdf/");

define("DEV_MODE","local");

// Customize Days
define("DAYS_LIST", serialize(array(1=>'Sun',2=>'Mon',3=>'Tue',4=>'Wed',5=>'Thu',6=>'Fri',7=>'Sat')));


?>
