<?php 

/***************************************************************
 *  File Name : Dasboard
 *  Created Date: 14/06/2016
 *  Created By: Prasad Bhale
 ************************************************************** */
/* Including Globally Declared Variables */
include("config/config.php");
$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array()
					  );

// Include Common Files
//include_once(CONFIG_CLASS_PATH ."class.php");

//Include Controller Section
//include(CONTROLLER_PATH."ManageUserController.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

?>



<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Desi Farm - Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/desi_farm_admin.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
	<style>
	.bg { 
	 

		/* The image used */
		/*background-image: url('images/desifarms_logo_clean (1).png');

	   
	}
	</style>
  </head>
  <body class="hold-transition login-page" >
    <div class="login-box" style="margin: 1% auto;">


      <div class="login-logo" style="margin: 0px;">
        <!--<a href="#" style="color:white;"><b>Admin</b> Login</a>-->
		
      </div><!-- /.login-logo -->
     <div class="login-logo" style="margin: 0px;margin-bottom:20px">
      
		<img src="images/logo.png" style="width: 100px;" >
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        
        <form name="login_frm" id="login_frm" action="dashboard.php" method="POST">
          <div class="form-group has-feedback">
            <input type="email" name="user_emailid" class="form-control" placeholder="Email" value=""  title="Please Enter Email ID">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="hidden" name="step" value="user_login">
            <input type="password" name="password" class="form-control" placeholder="Password" >
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
         
          <div class="row">
            <div class="col-sm-8">
              <div class="checkbox icheck">
                <label>
                  <!--<input type="checkbox"> Remember Me-->
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-sm-4">
              <!--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>-->
			  <input type="submit" class="btn btn-primary btn-block btn-flat" value="Sign In" name="submit" id="login_btn">
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
	 
    </div><!-- /.login-box -->

 <footer class="main-footer" style="margin-left:5px;margin-top:20px;">
	<div class="col-md-12">   
		
		<div class="col-md-12" style="text-align:center;color:#000"><strong>Copyright &copy; <?=date('Y');?> .</strong> All rights reserve at Newaves Technologies Pvt. Ltd.</div>
		<div class="col-md-5" style="">&nbsp;</div>
		
	</div>
	
</footer>
 <div class="col-md-4 col-md-offset-4"><?=$rec_msg;?></div>
<?php
$Messages[] = $rec_msg;	
$rec_msg='';?>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	<script type="text/javascript">
		jQuery(function($) {
		 $(".alert-success,.alert-notice,.alert-error,.alert-danger").animate({opacity:1.0}, 10000).fadeOut("slow");
		});

		
	</script>
    <script>
      $(function () {
	  
	 // $('#login_btn').click(function (e){
	
	//e.preventDefault();
	
	//alert('');
	//            $("#login_frm").submit();

	//})
	  
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
