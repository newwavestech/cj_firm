
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	<?php echo $title;?>
	<small><a class="btn btn-primary" href="create_acts.php">Create Act</a></small>
  </h1>
  <ol class="breadcrumb">
	<!--<li><a href="#" class="show"><i class="fa fa-search"></i> Search</a></li>-->
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#"><?php echo $title;?></a></li>
	
  </ol>
</section>
	
<!-- Main content -->
<section class="content">

  <div class="row">
	<div class="col-sm-12">
	  <div class="box">
		<div class="box-body ">
		
		  <table id="example2" class=" table-responsive table table-bordered table-hover">
			<thead>
			  <tr>
				<th width="5%">Sr No</th>
				<th width="15%">Act Title</th>
				<th width="5%">Action</th>
			  </tr>
			</thead>
			<tbody>
			<tr>
				<td>1</td>
				<td>Fundamental Act</td>
				<td>
					
					
					<a rel="#"> <i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;
					 <a href="#" ><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
					 <a rel="#" ><i class="fa fa-trash" aria-hidden="true"></i></a> 
				</td>
			  </tr>
			 
			<tbody>
			
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
