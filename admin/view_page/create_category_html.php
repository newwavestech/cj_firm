<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	<?php echo $sub_title;?>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#"><?php echo $title;?></a></li>
	<li><a href="#"><?php echo $sub_title;?></a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>


<!-- Main content -->

<section class="content">

			<form name="frm_product" id="frm_category" method="POST"  enctype="multipart/form-data">
			<input type="hidden" name="step" id="step" value="<?php if($category->category_id!="") {echo "update_category";}else{echo "add_new_category";}?>"/>
			<input   name="category_id" id="category_id" type="hidden" value="<?=$category->category_id;?>">
			
          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            
            <div class="box-body">
              <div class="row">
				<div class="col-md-12">
                  <div class="form-group">
                    <label>Category Title<span class="red">*</span></label>
					<input type="text" name="category_title" id="category_title" class="form-control" value="<?=$category->category_title;?>" required>					
                  </div><!-- /.form-group -->
				</div><!-- /.col -->
              
			  
			  
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
				
                    <a class="btn btn-default  pull-right" href="manage_category.php">Cancel</a>
                    <button class="btn btn-info pull-right" name="create_category" id="create_category" type="submit" style="margin-right:5px;">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div><!-- /.box -->
</form>
          <!-- /.row -->




        </section>


</div>
<!-- /.content-wrapper -->
