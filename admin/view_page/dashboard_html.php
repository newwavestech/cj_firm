  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Basic -->

    <div id="basic" class="well" style="max-width:44em;">
        <h4>Notification
            <a href="#" class="basic_close close pull-right" data-dismiss="alert" aria-label="close" title="close">�</a>
        </h4>
        <hr>
        <div id="notification_msg">
            <div class="alert alert-info fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">�</a>
                <strong>Info!</strong> This alert box indicates a neutral informative change or action.
            </div>
        </div>


    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <!--- Top Block To Display Counts  --->
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>200</h3>
                        <p>Total Acts</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-people-outline"></i>
                    </div>
                    <a href="manage_customers.php" class="small-box-footer">More info<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>400</h3>
                        <p>Total Sections</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-people-outline"></i>
                    </div>
                    <a href="manage_subscription.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div> <!-- ./col -->
        </div> <!-- ./row -->

        <!--- Top Block To Display Counts End--->

        <hr style="border-top : 1px dashed #000 !important;" />
        <!--########### Block For Today's Order ###############-->
		
		
    </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
