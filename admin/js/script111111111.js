
$(document).ready(function() {
	var notification_call = function(e) {
	
	$.ajax({
			type: 'POST', 
			datatype: 'data', 
			url: "controller/ajax/ajaxController.php",
			data : {'action':'notification'},
			success: function(result) 
			{
				
			var json = $.parseJSON(result);
			console.log(result);
			$("#notification_msg").text('');
			$(".notifications-menu a span").text(json.length);
			for(var i=0;i<json.length;i++){
			
			$("#notification_id").css("display","block");
				//console.log(json[i].notification_id+ "  "+json[i].content );
				//$("#notification_msg").append("<p>"+json[i].content+"</p>");
				$("#notification_msg").append("<div class='alert  fade in alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close' id="+json[i].notification_id+" title='close'>×</a>"+json[i].content+"</div>");
				
				}
				
			},
			error: function(result) {
			//alert(result);
			console.log(json);
			}
		});
  
};

 
var notification_hide = function() {
	
  $.ajax({
			type: 'POST', 
			datatype: 'data', 
			url: "controller/ajax/ajaxController.php",
			data : {'action':'notification','is_read':'1'},
			success: function(result) 
			{
				$("#notification_id").css("display","none");
					
				
			},
			error: function(result) {
			//alert(result);
			console.log(json);
			}
		});
  
};

//setInterval(notification_call, 50);
notification_call();
 
$("#notification_close").on("click", function(){
notification_hide();
});



});


 
 
 
 $(function () {
$('.datepicker').datepicker({
			format: "dd/mm/yyyy",
	  todayHighlight: true,
	  autoclose: true
	  
		});      });
	




$(document).ready(function() {


// Get Next Day's Delivery (MILK in Ltr)
$("#nextDayDelivery").on( "change", function (e){
		e.preventDefault();
		var nextDayDelivery=$(this).val();
		//alert(nextDayDelivery);
		$.ajax({
			type: 'POST', 
			datatype: 'json', 
			url: "controller/ajax/ajaxController.php",
			data : {'action':'next_day_delivery','nextDayDelivery':nextDayDelivery},
			success: function(result) 
			{
				
				var json = $.parseJSON(result);
			
				$("#Ngir_total_milk").text('0');  
				$("#Nbuffalo_total_milk").text('0'); 
				$("#Ncow_total_milk").text('0'); 
				$("#Ngold_total_milk").text('0'); 
				
				
				for(var i=0;i<json.length;i++){
				//console.log(json[i].milk_type+ "  "+json[i].total_milk );
				if(json[i].milk_type==1)
				$("#Ncow_total_milk").text(json[i].total_milk); 
				if(json[i].milk_type==2)
				$("#Nbuffalo_total_milk").text(json[i].total_milk); 
				if(json[i].milk_type==3)
				$("#Ngir_total_milk").text(json[i].total_milk); 
				if(json[i].milk_type==4)
				$("#Ngold_total_milk").text(json[i].total_milk); 
				}
				
			},
			error: function(result) {
			alert(result);
			}
		});
	 });
	



 /* Common Click Event For Form Validation 
	* By : Navnath Gaikwad
	* Created On : 26-04-2017
	*/	
	
	
	
	 $(".show").on( "click", function (e){
		e.preventDefault();
		$("#filter-panel-sub_daily").slideToggle("slower");
		$("#filter-panel-sub_alter").slideToggle("slower");
		$("#filter-panel-sub_weekly").slideToggle("slower");
		$("#filter-panel").slideToggle("slower");
		//$(this).text( $(this).text() == 'Show Filter' ? "Hide Filter" : "Show Filter"); // using ternary operator.
	 });
	
	// Edit User Info 
	  $('.edit_info').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");

			$("#sub_step").val("edit_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			 //alert(id+'   ppp '+submit_to);

      });

	
/*************** ADD and REMOVE product unit and Rate********************/

// ADD Product unit and rate
 $(".add_field_button").click(function (e) {
 	e.preventDefault();
		$(".unit_rate:last").clone().appendTo(".clone_data");
    });

	// REMOVE Product unit and rate
    $(".remove_field_button").click(function (e) {
 	e.preventDefault();
		if($(".unit_rate").length>1)
		$(".unit_rate:last").remove();
    });
	

/*************** Create User Form Validation Start ********************/	
$('#create_user').click(function (e){
	
	//e.preventDefault();
	
	  
	
            var user_type=$.trim($("#user_type").val());
             var user_full_name=$.trim($("#user_full_name").val());
             var user_emailid=$.trim($("#user_emailid").val());
             var user_contact_no=$.trim($("#user_contact_no").val());
             var user_address=$.trim($("#user_address").val());



            if(user_type=='' || user_type==null)
            {
            $("#user_type").next('div.red').remove();
            $("#user_type").after('<div class="red"> Select User Type</div>');
                            $("#user_type").focus();
            return false; 
            }else $("#user_type").next('div.red').remove();



            if(user_full_name=='' || user_full_name==null)
            {
            $("#user_full_name").next('div.red').remove();
            $("#user_full_name").after('<div class="red"> Enter User Full Name</div>');
                            $("#user_full_name").focus();
            return false; 
            }else $("#user_full_name").next('div.red').remove();


            if(user_emailid=='' || user_emailid==null)
            {
            $("#user_emailid").next('div.red').remove();
            $("#user_emailid").after('<div class="red"> Enter User Email ID </div>');
                            $("#user_emailid").focus();
            return false; 
            }else {
                     // validate customer_email id
                     var customer_emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    var valid = customer_emailReg.test(user_emailid);

                    if(!valid) {
                    $("#user_emailid").next('div.red').remove();
            $("#user_emailid").after('<div class="red">Enter Valid Email Address</div>');
                            $("#user_emailid").focus();
                              return false;
                    } else $("#user_emailid").next('div.red').remove();

                    }




            if(user_contact_no=='' || user_contact_no==null)
            {
            $("#user_contact_no").next('div.red').remove();
            $("#user_contact_no").after('<div class="red"> Enter User Mobile No </div>');
                            $("#user_contact_no").focus();
            return false; 
            }else $("#user_contact_no").next('div.red').remove();



            if(user_address=='' || user_address==null)
            {
            $("#user_address").next('div.red').remove();
            $("#user_address").after('<div class="red"> Enter User Address</div>');
                            $("#user_address").focus();
            return false; 
            }else $("#user_address").next('div.red').remove();
                   
    
           $("#frm_user").submit();
 
	});
	
/*************** Create User Form Validation End ********************/	
  
  		
		
		
/*************** Create Customer Form Validation Start ********************/	
$('#create_customer').click(function (e){
	
	e.preventDefault();
	
	     var customer_name=$.trim($("#customer_name").val());
		 var customer_email=$.trim($("#customer_email").val());
         var customer_mobile=$.trim($("#customer_mobile").val());
		 var customer_address=$.trim($("#customer_address").val());
		
		

		if(customer_name=='' || customer_name==null)
             {
                $("#customer_name").next('div.red').remove();
                $("#customer_name").after('<div class="red"> Enter Customer Name</div>');
				$("#customer_name").focus();
                return false; 
             }else $("#customer_name").next('div.red').remove();
	
	
		if(customer_email=='' || customer_email==null)
             {
                $("#customer_email").next('div.red').remove();
                $("#customer_email").after('<div class="red"> Enter Email ID </div>');
				$("#customer_email").focus();
                return false; 
             }else {
			 // validate customer_email id
			 var customer_emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			var valid = customer_emailReg.test(customer_email);

			if(!valid) {
			$("#customer_email").next('div.red').remove();
                $("#customer_email").after('<div class="red">Enter Valid Email Address</div>');
				$("#customer_email").focus();
				  return false;
			} else $("#customer_email").next('div.red').remove();
			
			}
			
			
	
		if(customer_mobile=='' || customer_mobile==null)
             {
                $("#customer_mobile").next('div.red').remove();
                $("#customer_mobile").after('<div class="red"> Enter Customer Mobile No </div>');
				$("#customer_mobile").focus();
                return false; 
             }else $("#customer_mobile").next('div.red').remove();
	
	
	
                if(customer_address=='' || customer_address==null)
             {
                $("#customer_address").next('div.red').remove();
                $("#customer_address").after('<div class="red"> Enter Customer Address</div>');
				$("#customer_address").focus();
                return false; 
             }else $("#customer_address").next('div.red').remove();
	
            $("#frm_customer").submit();
	});
	
/*************** Create Customer Form Validation End ********************/	
  
  
  
  	
   	
/*************** Create Product Form Validation Start ********************/	
$('#create_product').click(function (e){
	
	e.preventDefault();
	
	  
	     var product_unit=$.trim($("#product_unit").val());
		 var product_title=$.trim($("#product_title").val());
		 var product_category=$.trim($("#product_category").val());
		 var product_rate=$.trim($("#product_rate").val());
		 
		 var product_quantity=$.trim($("#product_quantity").val());
		 
		 var product_description=$.trim($("#product_description").val());
        
		
		if(product_title=='' || product_title==null)
             {
                $("#product_title").next('div.red').remove();
                $("#product_title").after('<div class="red"> Enter Product Title</div>');
				$("#product_title").focus();
                return false; 
             }else $("#product_title").next('div.red').remove();
	
		
		
		if(product_category=='' || product_category==null)
             {
                $("#product_category").next('div.red').remove();
                $("#product_category").after('<div class="red"> Select Product Category</div>');
				$("#product_category").focus();
                return false; 
             }else $("#product_category").next('div.red').remove();
	
		if(product_description=='' || product_description==null)
             {
                $("#product_description").next('div.red').remove();
                $("#product_description").after('<div class="red"> Enter User Mobile No </div>');
				$("#product_description").focus();
                return false; 
             }else $("#product_description").next('div.red').remove();
	
	
	

		if(product_unit=='' || product_unit==null)
             {
                $("#product_unit").next('div.red').remove();
                $("#product_unit").after('<div class="red"> Enter Product Unit</div>');
				$("#product_unit").focus();
                return false; 
             }else $("#product_unit").next('div.red').remove();
	
	
		
		if(product_rate=='' || product_rate==null)
             {
                $("#product_rate").next('div.red').remove();
                $("#product_rate").after('<div class="red"> Enter Product Rate</div>');
				$("#product_rate").focus();
                return false; 
             }else $("#product_rate").next('div.red').remove();
	
	
		if(product_quantity=='' || product_quantity==null)
             {
                $("#product_quantity").next('div.red').remove();
                $("#product_quantity").after('<div class="red"> Enter Product Unit Quantity</div>');
				$("#product_quantity").focus();
                return false; 
             }else $("#product_quantity").next('div.red').remove();
	
	
				$("#frm_product").submit();
 
	});
	
/*************** Create Product Form Validation End ********************/	


/*************** Check All Checkboxes ********************/	
  
  $("#checkAll").on("change",function(){
  if($('#checkAll').is(":checked"))
 {
	
    $('.incompleted').prop('checked', true);
 }
else
 {
    $('.incompleted').prop('checked', false);
 }  
  });

	


/************* Milk Variant Type Two cow and buffalo *****************/
$('#product_category').on('change', function() {
  
  if("Milk Varient"==$(this).find("option:selected").text()){
			$('#milk_category_div').css("display","block");
			
		}else{
			$('.milk_category').prop('checked', false);
			$('#milk_category_div').css("display","none");
			}
})
	
  
});






